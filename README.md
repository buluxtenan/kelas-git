# Kelas Git

Repo ini berisi skenario belajar git, meliputi

* rebase vs merge
* stash
* cherry pick
* tips

# Conflict Scenario

- create branch-1 dari main
  - edit berkas-conflicts.txt
  - create MR 1
- create branch-2 dari main
  - edit berkas-conflicts.txt
  - create MR 2
- merge MR 1

- fix MR 2
- git fech origin
- checkout branch-2 di lokal
- rebase -i main
- fix
- commit / git rebase --continue
- push --force
- merge MR 2

# Stash

- MR 2 Merged
- still on branch-2 at local
  - add line to berkas-conflicts.txt
  - save file
- checkout main
- git pull origin main
- will get error

- fix stash
- checkout to branch-2
- git stash
- git checkout main
- git stash apply
- git checkout -b branch-3
- git add, commit, push
- create MR

# Cherry pick

- create branch-3
  - add line to berkas-conflicts.txt
  - git add, commit,
  - add berkas cherry-pick.txt
  - git add, commit
  - save commit_hash from last commit
- create branch-4
  - plan to add cherry-pick.txt here

- fix cherry pick
- checkout branch-4
- git cherry-pick commit_hash_from_branch_3

- checkout branch-3
- git reset hard to commit_hash_before_cherry


# Dummy text
```
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non justo enim. Nunc tempor tortor et justo facilisis, at feugiat lorem feugiat. Sed aliquam tellus urna, et dignissim tellus ullamcorper non. Praesent facilisis diam eget leo ornare accumsan. Nulla sed volutpat mi. Aenean sed auctor orci. Etiam imperdiet magna eu quam volutpat gravida. Cras in viverra ex. Donec dignissim, augue vel porta dignissim, diam sem accumsan elit, at consectetur neque quam ut leo. Aliquam erat volutpat. Mauris tincidunt vulputate neque, sit amet lobortis eros aliquam at. Sed condimentum, velit elementum interdum luctus, dui est placerat tellus, bibendum feugiat lectus mauris sed ex. Praesent lobortis, elit ultrices efficitur fermentum, odio risus hendrerit erat, eget feugiat velit nibh ut lorem.

Curabitur dignissim sollicitudin sapien. Suspendisse vel facilisis tellus. Morbi mollis consequat ligula, ut aliquam est condimentum nec. Aliquam ornare ultrices enim, at viverra elit euismod quis. Praesent tincidunt dictum ornare. Sed dictum lorem sit amet posuere commodo. Suspendisse potenti. Cras in odio eget orci volutpat lobortis sit amet finibus ante. Praesent cursus dolor gravida est tincidunt, a gravida nisl scelerisque. Phasellus neque est, mattis vel tincidunt ac, consectetur sit amet sapien.

Ut egestas eu justo eget sollicitudin. Mauris sit amet ullamcorper dolor. Cras non enim leo. Pellentesque nisl arcu, porttitor et est feugiat, pretium tristique diam. Aenean congue tincidunt ligula a vulputate. Vivamus accumsan velit quis dolor interdum, vitae auctor turpis semper. Nam semper vel mi et facilisis. Suspendisse ultricies, libero at interdum suscipit, dolor urna congue orci, sit amet sollicitudin odio magna a justo. Pellentesque et sem eu elit ultricies tempor. Aenean feugiat odio vel commodo viverra. Curabitur eget risus sit amet mauris volutpat commodo nec et nibh. Aliquam sit amet efficitur purus, consectetur faucibus est. Sed efficitur sapien commodo lectus fermentum porta.

Praesent in nunc mauris. Quisque luctus felis vel magna gravida, eu posuere magna consequat. Praesent bibendum nisi massa, ac blandit enim blandit sed. Etiam et congue ipsum, nec pretium dui. Fusce accumsan lectus et tristique laoreet. Etiam vulputate erat sit amet justo fermentum porttitor. Curabitur suscipit, massa non imperdiet tincidunt, mi libero ultrices neque, sit amet egestas justo augue eget ipsum.

Cras sollicitudin massa tellus, et convallis lacus aliquam nec. In gravida felis id odio ornare tristique. Integer eros lacus, posuere at viverra vitae, elementum ut sem. Pellentesque in pharetra enim. Sed dictum lacinia lectus a efficitur. Suspendisse rutrum sit amet nibh at blandit. Integer id eros varius elit consectetur iaculis. 
```

# Tips

## Edit commit after push to origin
only ok if not in main branch

```
git checkout -b branch-amend
add berkas-amend.txt
git commit -m "ini sala commi"
git push origin branch-amend

```

Cara fix
```
git commit --amend
git push origin branch-amend --force
```

## Clean up
```
git gc --auto
git config --global fetch.prune true
git fetch origin
```
